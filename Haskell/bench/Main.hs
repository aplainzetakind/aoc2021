module Main (main) where

import qualified AoC2021.Day01 as D01
import qualified AoC2021.Day02 as D02
import qualified AoC2021.Day03 as D03
import qualified AoC2021.Day04 as D04
import qualified AoC2021.Day05 as D05
import qualified AoC2021.Day06 as D06
import qualified AoC2021.Day07 as D07
import qualified AoC2021.Day08 as D08
import qualified AoC2021.Day09 as D09
import qualified AoC2021.Day10 as D10
import qualified AoC2021.Day11 as D11
import qualified AoC2021.Day12 as D12
import qualified AoC2021.Day13 as D13
import qualified AoC2021.Day14 as D14
import Criterion.Main (bench, bgroup, defaultMain, nf)
import Data.Text (Text)
import qualified Data.Text.IO as TIO

evaluateAll :: [Text] -> [(String, String)]
evaluateAll =
  zipWith
    ($)
    [ D01.solve,
      D02.solve,
      D03.solve,
      D04.solve,
      D05.solve,
      D06.solve,
      D07.solve,
      D08.solve,
      D09.solve,
      D10.solve,
      D11.solve,
      D12.solve,
      D13.solve,
      D14.solve
    ]

main :: IO ()
main = do
  input1 <- TIO.readFile "../input/day1"
  input2 <- TIO.readFile "../input/day2"
  input3 <- TIO.readFile "../input/day3"
  input4 <- TIO.readFile "../input/day4"
  input5 <- TIO.readFile "../input/day5"
  input6 <- TIO.readFile "../input/day6"
  input7 <- TIO.readFile "../input/day7"
  input8 <- TIO.readFile "../input/day8"
  input9 <- TIO.readFile "../input/day9"
  input10 <- TIO.readFile "../input/day10"
  input11 <- TIO.readFile "../input/day11"
  input12 <- TIO.readFile "../input/day12"
  input13 <- TIO.readFile "../input/day13"
  input14 <- TIO.readFile "../input/day14"
  defaultMain
    [ bgroup
        "each"
        [ bench "1" $ nf D01.solve input1,
          bench "2" $ nf D02.solve input2,
          bench "3" $ nf D03.solve input3,
          bench "4" $ nf D04.solve input4,
          bench "5" $ nf D05.solve input5,
          bench "6" $ nf D06.solve input6,
          bench "7" $ nf D07.solve input7,
          bench "8" $ nf D08.solve input8,
          bench "9" $ nf D09.solve input9,
          bench "10" $ nf D10.solve input10,
          bench "11" $ nf D11.solve input11,
          bench "12" $ nf D12.solve input12,
          bench "13" $ nf D13.solve input13,
          bench "14" $ nf D14.solve input14
        ],
      bench "all" $
        nf
          evaluateAll
          [ input1,
            input2,
            input3,
            input4,
            input5,
            input6,
            input7,
            input8,
            input9,
            input10,
            input11,
            input12,
            input13,
            input14
          ]
    ]
