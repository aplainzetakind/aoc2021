module AoC2021.Day01 (solve) where

import AoC2021.Util.Types
import Data.Text (Text)
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer

solve :: Text -> (String, String)
solve input =
  let Just numbers = parseMaybe inputParser input
      s1 = show $ star1 numbers
      s2 = show $ star2 numbers
   in (s1, s2)

inputParser :: AoCParser [Int]
inputParser = manyTill (decimal <* eol) eof

offsetIncreases :: Int -> [Int] -> Int
offsetIncreases offset input =
  length
    . filter id
    . zipWith (>) (drop offset input)
    $ input

star1 :: [Int] -> Int
star1 = offsetIncreases 1

star2 :: [Int] -> Int
star2 = offsetIncreases 3
