{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module AoC2021.Day02 (solve) where

import AoC2021.Util.Types
import Data.List (foldl')
import Data.Text (Text)
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)

solve :: Text -> (String, String)
solve input =
  let Just commands = parseMaybe inputParser input
      (x, y1, y2) = run commands
      s1 = show $ x * y2
      s2 = show $ x * y1
   in (s1, s2)

data Command = Up Int | Down Int | Forward Int

inputParser :: AoCParser [Command]
inputParser = (`manyTill` eof) $ do
  c <- getCommand <$> many lowerChar
  space
  d <- decimal
  _ <- eol
  pure $ c d
  where
    getCommand = \case
      "up" -> Up
      "down" -> Down
      "forward" -> Forward
      _ -> error "parse failed"

run :: [Command] -> (Int, Int, Int)
run =
  let f (posx, posy, aim) = \case
        Up n -> (posx, posy, aim - n)
        Down n -> (posx, posy, aim + n)
        Forward n -> (posx + n, posy + aim * n, aim)
   in foldl' f (0, 0, 0)
