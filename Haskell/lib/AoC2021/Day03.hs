{-# LANGUAGE LambdaCase #-}

module AoC2021.Day03 (solve) where

import AoC2021.Util.Types
import Data.Bits
import Data.List (foldl')
import Data.Text (Text)
import Text.Megaparsec
import Text.Megaparsec.Char

solve :: Text -> (String, String)
solve input =
  let Just parsedInput = parseMaybe inputParser input
      s1 = show $ star1 parsedInput
      s2 = show $ star2 parsedInput
   in (s1, s2)

type ParsedInput = [[Int]]

inputParser :: AoCParser ParsedInput
inputParser = (`manyTill` eof) $ do
  ns <- fmap (\c -> read [c]) <$> many digitChar
  _ <- eol
  pure ns

bitsToInt :: [Int] -> Int
bitsToInt = foldl' (\res n -> shift res 1 + n) 0

mostCommonBits :: ParsedInput -> [Int]
mostCommonBits input =
  let counter = 0 <$ head input
      len = length input
      oneCounts = foldl' (zipWith (+)) counter input
   in (\n -> if 2 * n >= len then 1 else 0) <$> oneCounts

flipBit :: Int -> Int
flipBit n = if n == 1 then 0 else 1

data Filtering = ByMostCommon | ByLeastCommon

filterBits :: Filtering -> ParsedInput -> [Int]
filterBits filtering = go 0
  where
    go _ [ns] = ns
    go i nss =
      let f ByMostCommon = id
          f _ = flipBit
          matchbit = (f filtering <$> mostCommonBits nss) !! i
          nss' = filter (\ns -> ns !! i == matchbit) nss
       in go (i + 1) nss'

star1 :: ParsedInput -> Int
star1 input =
  let commons = mostCommonBits input
      gamma = bitsToInt commons
      epsilon = bitsToInt $ flipBit <$> commons
   in gamma * epsilon

star2 :: ParsedInput -> Int
star2 input =
  let oxyrate = bitsToInt $ filterBits ByMostCommon input
      co2rate = bitsToInt $ filterBits ByLeastCommon input
   in oxyrate * co2rate
