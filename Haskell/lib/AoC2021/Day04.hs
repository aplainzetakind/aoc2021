module AoC2021.Day04 where

import AoC2021.Util.Types
import Data.Map ((!))
import qualified Data.Map as M
import Data.Maybe (catMaybes, isNothing)
import Data.Text (Text)
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)

solve :: Text -> (String, String)
solve input =
  let Just parsedInput = parseMaybe inputParser input
      (s1, s2) = bothStars parsedInput
   in (show s1, show s2)

type ParsedInput = ([Int], [Board])

inputParser :: AoCParser ParsedInput
inputParser = do
  nums <- sepBy decimal (char ',') <* eol
  boards <- fmap (makeBoard . concat) <$> manyTill boardParser eof
  pure (nums, boards)
  where
    boardParser = eol *> many (hspace *> sepBy1 decimal hspace <* eol)

type Board =
  ( M.Map Int Int, -- mapping values to positions
    M.Map Int (Maybe Int) -- mapping positions to cell state
  )

makeBoard :: [Int] -> Board
makeBoard ns =
  let index = M.fromList . flip zip [0 ..] $ ns
      state = M.fromList . zip [0 ..] . fmap Just $ ns
   in (index, state)

winPatterns :: [[Int]]
winPatterns =
  [[n .. n + 4] | n <- [0, 5 .. 20]]
    ++ [[n, n + 5 .. n + 20] | n <- [0 .. 4]]

markBoard :: Int -> Board -> Board
markBoard n (i, s) = case M.lookup n i of
  Just p -> (i, M.insert p Nothing s)
  Nothing -> (i, s)

scoreBoard :: Board -> Int
scoreBoard =
  sum
    . catMaybes
    . dropWhile isNothing
    . fmap snd
    . M.toAscList
    . snd

checkBoard :: Board -> Maybe Int
checkBoard board =
  if any matchPattern winPatterns
    then Just $ scoreBoard board
    else Nothing
  where
    matchPattern :: [Int] -> Bool
    matchPattern xs = all isNothing $ (snd board !) <$> xs

bothStars :: ParsedInput -> (Int, Int)
bothStars (nums, boards) = go nums boards [] Nothing
  where
    go [] _ _ _ = error "Numbers exhausted"
    go (_ : ns) [] bs' ms1 = go ns bs' [] ms1
    go (n : ns) [b] [] (Just s1) =
      let b' = markBoard n b
       in case checkBoard b' of
            Just score -> (s1, score * n)
            Nothing -> go ns [b'] [] (Just s1)
    go (n : ns) (b : bs) bs' ms1 =
      let b' = markBoard n b
       in case (checkBoard b', ms1) of
            (Just _, Just _) -> go (n : ns) bs bs' ms1
            (Just score, _) ->
              go (n : ns) bs bs' $
                Just (score * n)
            _ -> go (n : ns) bs (b' : bs') ms1
