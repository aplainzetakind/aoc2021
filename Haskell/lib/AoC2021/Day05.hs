{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module AoC2021.Day05 where

import AoC2021.Util.Types
import Data.List (foldl', sortOn)
import qualified Data.Set as S
import Data.Text (Text)
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer

solve :: Text -> (String, String)
solve input =
  let Just parsedInput = parseMaybe inputParser input
      s1 = show $ star1 parsedInput
      s2 = show $ star2 parsedInput
   in (s1, s2)

type Point = (Int, Int)

type ParsedInput = [(Point, Point)]

inputParser :: AoCParser ParsedInput
inputParser = manyTill lineparser eof
  where
    lineparser = do
      x1 <- decimal
      _ <- char ','
      y1 <- decimal
      _ <- string " -> "
      x2 <- decimal
      _ <- char ','
      y2 <- decimal
      _ <- eol
      pure ((x1, y1), (x2, y2))

data Line
  = Vert Int (Int, Int)
  | Horz (Int, Int) Int
  | Slash (Int, Int) Int
  | Backslash (Int, Int) Int

makeLine :: Point -> Point -> Line
makeLine p1@(x1, _) p2@(x2, _) | x1 > x2 = makeLine p2 p1
makeLine (x1, y1) (x2, y2) | x1 == x2 = Vert x1 (min y1 y2, max y1 y2)
makeLine (x1, y1) (x2, y2) | y1 == y2 = Horz (min x1 x2, max x1 x2) y1
makeLine (x1, y1) (x2, y2) | y2 > y1 = Slash (x1, y1) (x2 - x1 + 1)
makeLine (x1, y1) (x2, _) | otherwise = Backslash (x1, y1) (x2 - x1 + 1)

smallerAbscissa :: Line -> Int
smallerAbscissa (Vert x _) = x
smallerAbscissa (Horz (x, _) _) = x
smallerAbscissa (Slash (x, _) _) = x
smallerAbscissa (Backslash (x, _) _) = x

greaterAbscissa :: Line -> Int
greaterAbscissa (Vert x _) = x
greaterAbscissa (Horz (_, x) _) = x
greaterAbscissa (Slash (x, _) n) = x + n - 1
greaterAbscissa (Backslash (x, _) n) = x + n - 1

intersectLine :: Line -> Line -> [Point]
intersectLine (Vert x1 (y1, y1')) (Vert x2 (y2, y2')) =
  if x1 == x2 then (x1,) <$> [max y1 y2 .. min y1' y2'] else []
intersectLine (Vert x1 (y1, y1')) (Horz (x2, x2') y2) =
  if y1 <= y2 && y2 <= y1' && x2 <= x1 && x1 <= x2'
    then [(x1, y2)]
    else []
intersectLine (Vert x1 (y1, y1')) (Slash (x2, y2) n) =
  let y = y2 - x2 + x1
   in if x2 <= x1 && x1 < x2 + n && y1 <= y && y <= y1'
        then [(x1, y)]
        else []
intersectLine (Vert x1 (y1, y1')) (Backslash (x2, y2) n) =
  let y = y2 + x2 - x1
   in if x2 <= x1 && x1 < x2 + n && y1 <= y && y <= y1'
        then [(x1, y)]
        else []
intersectLine (Horz (x1, x1') y1) (Horz (x2, x2') y2) =
  if y1 == y2 then (,y1) <$> [max x1 x2 .. min x1' x2'] else []
intersectLine (Horz (x1, x1') y1) (Slash (x2, y2) n) =
  let x = x2 - y2 + y1
   in if y2 <= y1 && y1 < y2 + n && x1 <= x && x <= x1'
        then [(x, y1)]
        else []
intersectLine (Horz (x1, x1') y1) (Backslash (x2, y2) n) =
  let x = y2 + x2 - y1
   in if y2 - n < y1 && y1 <= y2 && x1 <= x && x <= x1'
        then [(x, y1)]
        else []
intersectLine (Slash (x1, y1) m) (Slash (x2, y2) n) =
  let x = max x1 x2
      x' = min (x1 + m - 1) (x2 + n - 1)
   in if y1 - x1 == y2 - x2 && x <= x'
        then [(x + i, x + y1 - x1 + i) | i <- [0 .. x' - x]]
        else []
intersectLine (Slash (x1, y1) m) (Backslash (x2, y2) n) =
  let x = max x1 x2
      x' = min (x1 + m - 1) (x2 + n - 1)
      x0 = (x1 + x2 - y1 + y2) `quot` 2
      y0 = (-x1 + x2 + y1 + y2) `quot` 2
   in if even (x1 + y1 + x2 + y2) && x <= x0 && x0 <= x'
        then [(x0, y0)]
        else []
intersectLine (Backslash (x1, y1) m) (Backslash (x2, y2) n) =
  let x = max x1 x2
      x' = min (x1 + m - 1) (x2 + n - 1)
   in if y1 + x1 == y2 + x2 && x <= x'
        then [(x + i, -x + y1 + x1 - i) | i <- [0 .. x' - x]]
        else []
intersectLine l l' = intersectLine l' l

isOrthogonal :: (Point, Point) -> Bool
isOrthogonal ((x1, y1), (x2, y2)) = x1 == x2 || y1 == y2

star2 :: ParsedInput -> Int
star2 pps =
  let lns = sortOn smallerAbscissa . fmap (uncurry makeLine) $ pps
   in go lns S.empty 0
  where
    go [] set score = score + S.size set
    go (l : ls) set score =
      let candidates =
            takeWhile
              ( \line ->
                  smallerAbscissa line
                    <= greaterAbscissa l
              )
              ls
          newpoints = intersectLine l =<< candidates
          (set0, set1) =
            S.spanAntitone
              ((< smallerAbscissa l) . fst)
              set
          score' = score + S.size set0
          set' = foldl' (flip S.insert) set1 newpoints
       in go ls set' score'

star1 :: ParsedInput -> Int
star1 = star2 . filter isOrthogonal
