{-# LANGUAGE TupleSections #-}

module AoC2021.Day06 where

import AoC2021.Util.Types (AoCParser)
import Data.List (foldl')
import qualified Data.Map.Lazy as M
import Data.Matrix as Matrix
import Data.Text (Text)
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer (decimal)

solve :: Text -> (String, String)
solve input =
  let Just parsedInput = parseMaybe inputParser input
      s1 = show $ star1 parsedInput
      s2 = show $ star2 parsedInput
   in (s1, s2)

type ParsedInput = [Int]

inputParser :: AoCParser ParsedInput
inputParser = sepBy decimal (char ',') <* eol <* eof

buildVector :: [Int] -> Matrix.Matrix Int
buildVector =
  Matrix.fromList 9 1
    . fmap snd
    . M.toAscList
    . foldl'
      (\m n -> M.insertWith (+) n 1 m)
      (M.fromList $ (,0) <$> [0 .. 8])

dayMatrix :: Matrix.Matrix Int
dayMatrix =
  Matrix.matrix
    9
    9
    ( \(i, j) ->
        if j - i == 1
          || (i, j) == (7, 1)
          || (i, j) == (9, 1)
          then 1
          else 0
    )

countAfterDays :: Int -> ParsedInput -> Int
countAfterDays n = sum . Matrix.multStd (dayMatrix ^ n) . buildVector

star1 :: ParsedInput -> Int
star1 = countAfterDays 80

star2 :: ParsedInput -> Int
star2 = countAfterDays 256
