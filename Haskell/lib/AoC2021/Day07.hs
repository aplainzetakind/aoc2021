{-# OPTIONS_GHC -Wno-type-defaults #-}

module AoC2021.Day07 where

import AoC2021.Util.Types (AoCParser)
import Data.Text (Text)
import Text.Megaparsec
import Text.Megaparsec.Char (char, eol)
import Text.Megaparsec.Char.Lexer (decimal)

solve :: Text -> (String, String)
solve input =
  let Just parsedInput = parseMaybe inputParser input
      s1 = show $ star1 parsedInput
      s2 = show $ star2 parsedInput
   in (s1, s2)

type ParsedInput = [Int]

inputParser :: AoCParser ParsedInput
inputParser = sepBy decimal (char ',') <* eol <* eof

median :: Fractional a => [Int] -> a
median [] = error "median: empty"
median ns =
  let len = length ns
      half = len `quot` 2
   in if even len
        then fromIntegral (go half ns + go (half + 1) ns) / 2
        else fromIntegral $ go (half + 1) ns
  where
    go i ms =
      let k = ms !! (i - 1)
          lesser = filter (< k) ms
          greater = filter (> k) ms
          equal = filter (== k) ms
          result
            | length lesser < i
                && length lesser + length equal >= i =
                k
            | length lesser < i =
                go
                  (i - length lesser - length equal)
                  greater
            | otherwise = go i lesser
       in result

star1 :: ParsedInput -> Int
star1 ns =
  let m = floor . median $ ns
   in sum . fmap (\n -> abs (n - m)) $ ns

star2 :: ParsedInput -> Int
star2 ns =
  let meanfloor = sum ns `quot` length ns
      distcost n = (n * (n + 1)) `quot` 2
      tcost p0 = sum $ fmap (\p -> distcost (abs (p - p0))) ns
   in min (tcost meanfloor) . tcost $ meanfloor + 1
