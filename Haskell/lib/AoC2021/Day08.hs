{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module AoC2021.Day08 where

import AoC2021.Util.Types (AoCParser)
import Data.List (sort)
import Data.Map ((!))
import qualified Data.Map as M
import Data.Text (Text)
import Text.Megaparsec
import Text.Megaparsec.Char
  ( char,
    eol,
    letterChar,
    string,
  )

solve :: Text -> (String, String)
solve input =
  let Just parsedInput = parseMaybe inputParser input
      s1 = show $ star1 parsedInput
      s2 = show $ star2 parsedInput
   in (s1, s2)

type ParsedInput = [([String], [String])]

inputParser :: AoCParser ParsedInput
inputParser = manyTill lineParser eof
  where
    lineParser = do
      ss <- endBy (many letterChar) (char ' ')
      _ <- string "| "
      ss' <- sepBy (many letterChar) (char ' ')
      _ <- eol
      pure (ss, ss')

star1 :: ParsedInput -> Int
star1 = length . filter (`elem` [2, 3, 4, 7]) . fmap length . concatMap snd

allCellPatterns :: [String]
allCellPatterns =
  [ "abcefg",
    "cf",
    "acdeg",
    "acdfg",
    "bcdf",
    "abdfg",
    "abdefg",
    "acf",
    "abcdefg",
    "abcdfg"
  ]

cellsToDigit :: String -> Char
cellsToDigit = \case
  "abcefg" -> '0'
  "cf" -> '1'
  "acdeg" -> '2'
  "acdfg" -> '3'
  "bcdf" -> '4'
  "abdfg" -> '5'
  "abdefg" -> '6'
  "acf" -> '7'
  "abcdefg" -> '8'
  "abcdfg" -> '9'
  p -> error $ "unknown pattern: " ++ p

isCellSubset :: String -> String -> Bool
isCellSubset cs cs' = all (`elem` cs') cs

cellDiff :: String -> String -> String
cellDiff cs cs' = filter (not . (`elem` cs')) cs

eliminateSubsetIncompatibility :: M.Map String [String] -> M.Map String [String]
eliminateSubsetIncompatibility mp =
  let keys = M.keys . M.filter ((== 1) . length) $ mp
      flt m k k' ss =
        if k `isCellSubset` k'
          then filter (\s -> any (`isCellSubset` s) (m ! k)) ss
          else ss
      folder m k = M.mapWithKey (flt m k) m
   in foldl folder mp keys

generateDifferences :: M.Map String [String] -> M.Map String [String]
generateDifferences mp =
  let keys = M.keys . M.filter ((== 1) . length) $ mp
      folder m k =
        let mp2 = M.filterWithKey (\k' _ -> k `isCellSubset` k') mp
            ks = head $ mp ! k
            mp3 = M.map (fmap (`cellDiff` ks)) . M.mapKeys (`cellDiff` k) $ mp2
         in M.union mp3 m
   in foldl folder mp keys

solveMap :: [String] -> M.Map String [String] -> M.Map String String
solveMap ss mp | all ((== 1) . length . (mp !)) ss = fmap head mp
solveMap ss mp =
  let mp' = generateDifferences . eliminateSubsetIncompatibility $ mp
   in solveMap ss mp'

decodeLine :: ([String], [String]) -> Int
decodeLine (ss, ss') =
  let (srt, srt') = (fmap sort ss, fmap sort ss')
      strings = srt ++ srt'
      smap =
        solveMap srt'
          . M.fromList
          $ (\s -> (s, filter ((== length s) . length) allCellPatterns))
            <$> strings
      unscramble = (smap !)
   in read $ fmap (cellsToDigit . unscramble) srt'

star2 :: ParsedInput -> Int
star2 = sum . fmap decodeLine
