{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}

module AoC2021.Day09 where

import AoC2021.Util.Types (AoCParser)
import Data.Array
import qualified Data.IntSet as S
import Data.List (foldl', sortOn)
import Data.Ord (Down (..))
import Data.Text (Text)
import Text.Megaparsec
import Text.Megaparsec.Char
  ( digitChar,
    eol,
  )

solve :: Text -> (String, String)
solve input =
  let Just (arr, m, n) = makeArray <$> parseMaybe inputParser input
      s1 = show $ star1 arr m n
      s2 = show $ star2 arr m n
   in (s1, s2)

type ParsedInput = [[Int]]

inputParser :: AoCParser ParsedInput
inputParser = manyTill (manyTill (read . pure <$> digitChar) eol) eof

neighbours :: Int -> Int -> (Int, Int) -> [(Int, Int)]
neighbours m n (i, j) = filter f [(i, j - 1), (i - 1, j), (i + 1, j), (i, j + 1)]
  where
    f (k, l) = 0 <= k && k < n && 0 <= l && l < m

neighbours' :: Int -> Int -> Int -> [Int]
neighbours' m n k =
  let (i, j) = k `divMod` n
   in (\(a, b) -> a * n + b) <$> neighbours m n (i, j)

makeArray :: [[Int]] -> (Array Int Int, Int, Int)
makeArray nss =
  let m = length nss
      n = length $ head nss
   in (listArray (0, n * m - 1) $ concat nss, m, n)

star1 :: Array Int Int -> Int -> Int -> Int
star1 arr m n =
  let nbrs = neighbours' m n
      f k =
        let value = arr ! k
         in if all (> value) $ (arr !) <$> nbrs k
              then value + 1
              else 0
   in sum . fmap f $ indices arr

star2 :: Array Int Int -> Int -> Int -> Int
star2 arr m n =
  let coords = S.fromList $ filter ((/= 9) . (arr !)) $ indices arr
      go cs _ _ bs | S.null cs = product . take 3 $ sortOn Down bs
      go cs [] qs bs = go (S.deleteMin cs) [S.findMin cs] [] (length qs : bs)
      go cs (p : ps) qs bs =
        let ps' = filter (`S.member` cs) $ neighbours' m n p
            cs' = foldl' (flip S.delete) cs ps'
         in go cs' (ps' ++ ps) (p : qs) bs
   in go coords [] [] []
