{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
{-# OPTIONS_GHC -Wno-type-defaults #-}

module AoC2021.Day10 where

import AoC2021.Day07 (median)
import AoC2021.Util.Types (AoCParser)
import Data.List (foldl')
import Data.Maybe (mapMaybe)
import Data.Text (Text)
import Text.Megaparsec
import Text.Megaparsec.Char
  ( eol,
    printChar,
  )

solve :: Text -> (String, String)
solve input =
  let Just parsedInput = parseMaybe inputParser input
      s1 = show $ star1 parsedInput
      s2 = show $ star2 parsedInput
   in (s1, s2)

type ParsedInput = [String]

openDelims :: String
openDelims = "([{<"

isOpenDelim :: Char -> Bool
isOpenDelim = (`elem` openDelims)

closingDelim :: Char -> Char
closingDelim = \case
  '(' -> ')'
  '[' -> ']'
  '{' -> '}'
  '<' -> '>'
  x -> error $ "Unexpected " ++ show x

completeScoreDelim :: Char -> Int
completeScoreDelim = \case
  ')' -> 1
  ']' -> 2
  '}' -> 3
  '>' -> 4

errorScoreDelim :: Char -> Int
errorScoreDelim = \case
  ')' -> 3
  ']' -> 57
  '}' -> 1197
  '>' -> 25137

inputParser :: AoCParser ParsedInput
inputParser = manyTill (manyTill printChar eol) eof

scoreLine1 :: String -> Maybe Int
scoreLine1 = go []
  where
    go _ [] = Nothing
    go ys (x : xs) | isOpenDelim x = go (closingDelim x : ys) xs
    go (y : ys) (x : xs) | x == y = go ys xs
    go _ (x : _) = Just $ errorScoreDelim x

scoreLine2 :: String -> Maybe Int
scoreLine2 = go []
  where
    go [] [] = Nothing
    go ys [] = Just $ foldl' (\x y -> 5 * x + y) 0 . fmap completeScoreDelim $ ys
    go ys (x : xs) | isOpenDelim x = go (closingDelim x : ys) xs
    go (y : ys) (x : xs) | x == y = go ys xs
    go _ _ = Nothing

star1 :: [String] -> Int
star1 = sum . mapMaybe scoreLine1

star2 :: [String] -> Int
star2 = floor . median . mapMaybe scoreLine2
