{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}

module AoC2021.Day11 where

import AoC2021.Util.Types (AoCParser)
import Control.Monad.ST (ST, runST)
import Data.Array.IArray
import Data.Array.ST
import Data.Foldable (traverse_)
import Data.Maybe (isNothing)
import Data.Text (Text)
import Text.Megaparsec
import Text.Megaparsec.Char
  ( digitChar,
    eol,
  )

solve :: Text -> (String, String)
solve input =
  let Just parsedInput = parseMaybe inputParser input
      s1 = show $ star1 parsedInput
      s2 = show $ star2 parsedInput
   in (s1, s2)

type ParsedInput = [[Int]]

inputParser :: AoCParser ParsedInput
inputParser = manyTill (manyTill (read . pure <$> digitChar) eol) eof

buildArray :: [[Int]] -> (forall s. ST s (STUArray s (Int, Int) Int))
buildArray nss =
  let w = length . head $ nss
      h = length nss
   in newListArray ((0, 0), (w - 1, h - 1)) $ concat nss

constArray :: Ix i => (i, i) -> Int -> ST s (STUArray s i Int)
constArray r n = newListArray r $ repeat n

neighbours :: ((Int, Int), (Int, Int)) -> (Int, Int) -> [(Int, Int)]
neighbours b (i, j) =
  let block = range ((i - 1, j - 1), (i + 1, j + 1)) :: [(Int, Int)]
   in filter (\q -> b `inRange` q && q /= (i, j)) block

elapseStep :: STUArray s (Int, Int) Int -> ST s Int
elapseStep arr = do
  arrbounds <- getBounds arr
  let flash p a =
        traverse_ (\q -> readArray a q >>= writeArray a q . (+ 1)) $
          neighbours arrbounds p
      addandcarry a2 carry p = do
        v <- readArray arr p
        v' <- readArray a2 p
        let r = v + v'
        writeArray arr p r
        if v <= 9 && r > 9
          then do
            flash p carry
            pure True
          else pure False
      go a1 = do
        carry <- constArray arrbounds 0
        changed <-
          fmap or
            . traverse (addandcarry a1 carry)
            . range
            =<< getBounds a1
        if changed then go carry else pure ()
      zeroout =
        traverse_
          ( \p ->
              readArray arr p
                >>= ( \x ->
                        if x > 9
                          then writeArray arr p 0
                          else pure ()
                    )
          )
          $ range arrbounds
  c1 <- constArray arrbounds 1
  go c1
  zeroout
  points <- range <$> getBounds arr
  fmap sum
    . traverse
      ( \p ->
          readArray arr p
            >>= ( \x ->
                    if x == 0
                      then pure 1
                      else pure 0
                )
      )
    $ points

bothStars :: ParsedInput -> (Int, Int)
bothStars nss = runST $
  do
    arr <- buildArray nss
    arrbounds <- getBounds arr
    let full = rangeSize arrbounds
        go _ (Just s1) (Just s2) _ = pure (s1, s2)
        go n ms1 ms2 s1total =
          do
            k <- elapseStep arr
            let s1total' = if isNothing ms1 then k + s1total else 0
                ms1' = ms1 <|> if n == 100 then Just s1total' else Nothing
                ms2' = ms2 <|> if k == full then Just n else Nothing
            go (n + 1) ms1' ms2' s1total'
    go 1 Nothing Nothing 0

star1 :: ParsedInput -> Int
star1 = fst . bothStars

star2 :: ParsedInput -> Int
star2 = snd . bothStars
