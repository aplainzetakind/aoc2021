{-
   TODO: Encode cave sets by numbers and memoize with an array
-}
{-# LANGUAGE OverloadedStrings #-}

module AoC2021.Day12 where

import AoC2021.Util.Memo (memoizeM)
import AoC2021.Util.Types (AoCParser)
import Data.Bits
import Data.Char (isAsciiLower)
import qualified Data.IntMap as IM
import Data.List (foldl', nub, sort)
import qualified Data.Map.Lazy as M
import qualified Data.Set as S
import Data.Text (Text)
import Data.Tuple (swap)
import Text.Megaparsec hiding (State)
import Text.Megaparsec.Char

solve :: Text -> (String, String)
solve input =
  let Just processedInput = processInput <$> parseMaybe inputParser input
      s1 = show $ star1 processedInput
      s2 = show $ star2 processedInput
   in (s1, s2)

type ParsedInput = [(String, String)]

inputParser :: AoCParser ParsedInput
inputParser = manyTill lineParser eof
  where
    lineParser = do
      c1 <- manyTill letterChar (char '-')
      c2 <- manyTill letterChar eol
      pure (c1, c2)

data Cave = Start | Small String | Big String | End deriving (Eq, Ord)

instance Show Cave where
  show Start = "start"
  show (Big s) = s
  show (Small s) = s
  show End = "end"

mkCave :: String -> Cave
mkCave [] = error "Empty string"
mkCave "start" = Start
mkCave "end" = End
mkCave s@(c : _) = if isAsciiLower c then Small s else Big s

isBig :: Cave -> Bool
isBig (Big _) = True
isBig _ = False

type CaveMap = IM.IntMap (S.Set Int)

processInput :: ParsedInput -> (CaveMap, Int, Int)
processInput input =
  let allCaves =
        sort
          . nub
          . concatMap
            ( \(a, b) ->
                [mkCave a, mkCave b]
            )
          $ input
      zipped = zip allCaves [0 ..]
      bigmask =
        foldl' (.|.) 0
          . fmap (bit . snd)
          . filter (isBig . fst)
          $ zipped
      index = M.fromList zipped
      f m (s1, s2) =
        IM.insertWith
          S.union
          (index M.! mkCave s1)
          (S.singleton $ index M.! mkCave s2)
          m
      cavemap = foldl f IM.empty (input ++ fmap swap input)
      end = length allCaves - 1
   in (cavemap, end, bigmask)

countPathsM ::
  Monad m =>
  (CaveMap, Int, Int) ->
  ((Int, Int) -> m Int) ->
  ((Int, Int) -> m Int)
countPathsM (cmp, end, _) _ (n, _) | n == end = pure 1
countPathsM (cmp, _, bmsk) f (cave, seen) =
  do
    let seen' = if bit cave .&. bmsk /= 0 then seen else bit cave .|. seen
        nbrs = S.toList $ S.filter (\c -> bit c .&. seen == 0) (cmp IM.! cave)
    sum <$> traverse (\c -> f (c, seen')) nbrs

countPathsM' ::
  Monad m =>
  (CaveMap, Int, Int) ->
  ((Int, Int, Bool) -> m Int) ->
  ((Int, Int, Bool) -> m Int)
countPathsM' (cmp, end, _) _ (n, _, _) | n == end = pure 1
countPathsM' (cmp, _, bmsk) f (cave, seen, doubled) =
  do
    let seen' = if bit cave .&. bmsk /= 0 then seen else bit cave .|. seen
        nbrs0 = cmp IM.! cave
        nbrs =
          if doubled
            then S.filter (\c -> bit c .&. seen == 0) nbrs0
            else S.filter (/= 0) nbrs0
        g c =
          let doubled' = doubled || bit c .&. seen /= 0
           in f (c, seen', doubled')
    sum <$> traverse g (S.toList nbrs)

countPaths :: (CaveMap, Int, Int) -> (Int, Int) -> Int
countPaths = memoizeM . countPathsM

countPaths' :: (CaveMap, Int, Int) -> (Int, Int, Bool) -> Int
countPaths' = memoizeM . countPathsM'

star1 :: (CaveMap, Int, Int) -> Int
star1 input = countPaths input (0, 1)

star2 :: (CaveMap, Int, Int) -> Int
star2 input = countPaths' input (0, 1, False)

test1 = "start-A\nstart-b\nA-c\nA-b\nb-d\nA-end\nb-end\n"
