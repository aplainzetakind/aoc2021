{-# LANGUAGE OverloadedStrings #-}

module AoC2021.Day13 where

import AoC2021.Util.Types (AoCParser)
import Control.Arrow (first, second)
import qualified Data.Set as S
import Data.Text (Text)
import Debug.Trace
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer

solve :: Text -> (String, String)
solve input =
  let Just parsedInput = parseMaybe inputParser input
      s1 = show $ star1 parsedInput
      s2 = star2 parsedInput
   in (s1, s2)

data Fold = X Int | Y Int deriving (Show)

type ParsedInput = ([(Int, Int)], [Fold])

inputParser :: AoCParser ParsedInput
inputParser = do
  points <- manyTill coordParser eol
  folds <- manyTill foldParser eof
  pure (points, folds)
  where
    coordParser = do
      x <- decimal
      _ <- char ','
      y <- decimal
      _ <- eol
      pure (x, y)
    foldParser :: AoCParser Fold
    foldParser = do
      _ <- string "fold along "
      c <- letterChar
      _ <- char '='
      dist <- decimal
      _ <- eol
      pure $ case c of
        'x' -> X dist
        'y' -> Y dist
        _ -> error "no parse"

paperFold :: Fold -> S.Set (Int, Int) -> S.Set (Int, Int)
paperFold f = S.map $ g (\n -> if n > dist then 2 * dist - n else n)
  where
    (g, dist) = case f of
      X d -> (first, d)
      Y d -> (second, d)

render :: S.Set (Int, Int) -> String
render s =
  let ys = S.map snd s
      (maxx, _) = S.findMax s
      maxy = S.findMax ys
   in unlines
        [ [if (i, j) `S.member` s then '#' else '.' | i <- [0 .. maxx]]
          | j <- [0 .. maxy]
        ]

star1 :: ParsedInput -> Int
star1 (points, (f : _)) = S.size . paperFold f $ S.fromList points

star2 :: ParsedInput -> String
star2 (points, fs) =
  let finalpoints = foldl (flip paperFold) (S.fromList points) fs
   in render finalpoints
