{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module AoC2021.Day14 where

import AoC2021.Util.Memo (memoizeM)
import AoC2021.Util.Types (AoCParser)
import Data.List (foldl', group, sort)
import qualified Data.Map as M
import Data.Text (Text)
import Text.Megaparsec
import Text.Megaparsec.Char

solve :: Text -> (String, String)
solve input =
  let Just processedInput = processInput <$> parseMaybe inputParser input
      s1 = show $ star1' processedInput
      s2 = show $ star2' processedInput
   in (s1, s2)

type ParsedInput = (String, [(String, Char)])

inputParser :: AoCParser ParsedInput
inputParser = do
  start <- manyTill letterChar eol
  _ <- eol
  transfs <- manyTill lineParser eof
  pure (start, transfs)
  where
    lineParser = do
      inp <- many letterChar
      _ <- string " -> "
      out <- letterChar
      _ <- eol
      pure (inp, out)

processInput :: ParsedInput -> (String, M.Map String Char)
processInput = fmap M.fromList

splitPairings :: String -> [String]
splitPairings (c : c' : cs) = [c, c'] : splitPairings (c' : cs)
splitPairings s = []

mergeResults ::
  (Char, M.Map Char Int, Char) ->
  (Char, M.Map Char Int, Char) ->
  (Char, M.Map Char Int, Char)
mergeResults (_, _, c1') (c2, _, _) | c1' /= c2 = error "incompatible neighbours"
mergeResults (c1, m1, c1') (c2, m2, c2') =
  let m = M.foldrWithKey' f m1 m2
      f k v mp = M.insertWith (+) k v mp
      m' = M.insertWith (+) c1' 1 m
   in (c1, m', c2')

evolveM ::
  Monad m =>
  M.Map String Char ->
  ((Int, String) -> m (Char, M.Map Char Int, Char)) ->
  ((Int, String) -> m (Char, M.Map Char Int, Char))
evolveM _ _ (0, s) =
  pure
    ( head s,
      M.fromListWith
        (+)
        ( (,1)
            <$> (tail $ init s)
        ),
      last s
    )
evolveM mp f (n, s) = do
  nexts <-
    traverse (\s -> f (n - 1, s))
      . concatMap (applyTrans mp)
      $ splitPairings s
  pure $ foldl1 mergeResults nexts

evolve :: M.Map String Char -> Int -> String -> (Char, M.Map Char Int, Char)
evolve = curry . memoizeM . evolveM

mergePairings :: [String] -> String
mergePairings (s : ss) = s ++ concatMap tail ss

applyTrans :: M.Map String Char -> String -> [String]
applyTrans mp s@[c1, c2] = case M.lookup s mp of
  Just c -> [[c1, c], [c, c2]]
  Nothing -> [s]

evolveAndCount :: Int -> (String, M.Map String Char) -> Int
evolveAndCount n (s, mp) =
  let (c, m, c') = evolve mp n s
      m' = M.insertWith (+) c 1 . M.insertWith (+) c' 1 $ m
      counts = sort . fmap snd $ M.toList m'
   in last counts - head counts

star1' = evolveAndCount 10

star2' = evolveAndCount 40

star1 :: (String, M.Map String Char) -> Int
star1 (s, mp) =
  let res =
        mergePairings
          . (!! 10)
          . iterate (concatMap $ applyTrans mp)
          $ splitPairings s
      counts = sort . fmap length . group . sort $ res
   in last counts - head counts

star2 :: (String, M.Map String Char) -> Int
star2 (s, mp) =
  let res =
        mergePairings
          . (!! 40)
          . iterate (concatMap $ applyTrans mp)
          $ splitPairings s
      counts = sort . fmap length . group . sort $ res
   in last counts - head counts
