module AoC2021.Day12 where

import AoC2021.Util.Types (AoCParser)
import Data.Text (Text)
import Text.Megaparsec
import Text.Megaparsec.Char

solve :: Text -> (String, String)
solve input =
  let Just parsedInput = parseMaybe inputParser input
      s1 = show $ star1 parsedInput
      s2 = show $ star2 parsedInput
   in (s1, s2)

type ParsedInput = [[Int]]

inputParser :: AoCParser ParsedInput
inputParser = undefined

star1 :: ParsedInput -> Int
star1 = const 1

star2 :: ParsedInput -> Int
star2 = const 2
