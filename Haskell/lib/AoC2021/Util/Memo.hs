module AoC2021.Util.Memo where

import Control.Monad.Identity
import Control.Monad.State
import Data.Array
import qualified Data.Map as M

type StateMap a b = State (M.Map a b) b

memoizeM :: Ord a => ((a -> StateMap a b) -> (a -> StateMap a b)) -> (a -> b)
memoizeM t x = evalState (f x) M.empty
  where
    g z = do
      y <- t f z
      m <- get
      put $ M.insert z y m
      return y
    f y = get >>= \m -> maybe (g y) return (M.lookup y m)

memoizeA :: Ix a => (a, a) -> ((a -> b) -> (a -> b)) -> Array a b
memoizeA b t = a
  where
    a = listArray b [t f x | x <- range b]
    f x
      | inRange b x = a ! x
      | otherwise = t f x

memoizeArray :: Ix a => (a, a) -> ((a -> b) -> (a -> b)) -> (a -> b)
memoizeArray b t x = memoizeA b t ! x

unidentity ::
  ((a -> Identity b) -> (a -> Identity b)) ->
  ((a -> b) -> (a -> b))
unidentity f g = (runIdentity .) $ f (pure . g)
