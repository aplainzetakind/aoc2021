module AoC2021.Util.Types where

import Data.Text as T
import Data.Void (Void)
import Text.Megaparsec (Parsec)

type AoCParser = Parsec Void T.Text
