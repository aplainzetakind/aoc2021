{-# LANGUAGE LambdaCase #-}

module Main where

import qualified AoC2021.Day01 as Day01
import qualified AoC2021.Day02 as Day02
import qualified AoC2021.Day03 as Day03
import qualified AoC2021.Day04 as Day04
import qualified AoC2021.Day05 as Day05
import qualified AoC2021.Day06 as Day06
import qualified AoC2021.Day07 as Day07
import qualified AoC2021.Day08 as Day08
import qualified AoC2021.Day09 as Day09
import qualified AoC2021.Day10 as Day10
import qualified AoC2021.Day11 as Day11
import qualified AoC2021.Day12 as Day12
import qualified AoC2021.Day13 as Day13
import qualified AoC2021.Day14 as Day14
import Data.Text (Text)
import qualified Data.Text.IO as TIO
import System.Environment (getArgs)

maxDay :: Int
maxDay = 14

run :: Int -> Text -> (String, String)
run = \case
  1 -> Day01.solve
  2 -> Day02.solve
  3 -> Day03.solve
  4 -> Day04.solve
  5 -> Day05.solve
  6 -> Day06.solve
  7 -> Day07.solve
  8 -> Day08.solve
  9 -> Day09.solve
  10 -> Day10.solve
  11 -> Day11.solve
  12 -> Day12.solve
  13 -> Day13.solve
  14 -> Day14.solve
  _ -> error "Invalid day"

main :: IO ()
main = do
  day <- (read :: String -> Int) . head <$> getArgs
  if day > maxDay
    then putStrLn $ "Day " ++ show day ++ " not yet completed"
    else do
      input <- TIO.readFile $ "../input/day" ++ show day
      let (star1, star2) = run day input
      putStrLn star1
      putStrLn star2
