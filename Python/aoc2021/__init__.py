import time

import aoc2021.days.day01
import aoc2021.days.day02
import aoc2021.days.day03
import aoc2021.days.day04
import aoc2021.days.day05
import aoc2021.days.day06
import aoc2021.days.day07
import click


def render_time(time: float) -> str:
    if time > 1:
        return f"{time:.2f}s"
    time *= 1000
    if time > 1:
        return f"{int(time)}ms"
    time *= 1000
    if time > 1:
        return f"{int(time)}μs"
    time *= 1000
    if time > 1:
        return f"{int(time)}ns"


@click.command()
@click.argument("day")
def main(day):
    day = str(int(day))
    day_padded = day.zfill(2)
    day_module = getattr(aoc2021.days, "day" + day_padded)
    with open("../input/day" + day) as f:
        input = f.read()
    t0 = time.perf_counter()
    (star1, star2) = day_module.solve(input)
    print(star1)
    print(star2)
    ttime = time.perf_counter() - t0
    print(f"Day {day} computed in {render_time(ttime)}")
