def offsetIncreses(offset: int, nums: [int]) -> int:
    count = 0

    for i in range(offset, len(nums)):
        if nums[i] > nums[i - offset]:
            count += 1

    return count


def solve(input: str) -> (str, str):
    input = [int(line) for line in input.strip().split("\n")]
    star1 = offsetIncreses(1, input)
    star2 = offsetIncreses(3, input)

    return (star1, star2)
