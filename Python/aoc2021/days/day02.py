def solve(input: str) -> (str, str):
    input = input.strip().split("\n")

    x_dist = 0
    y_dist = 0
    aim = 0

    for line in input:
        command, dist = line.split()
        dist = int(dist)
        if command == "forward":
            x_dist += dist
            y_dist += aim * dist
        elif command == "up":
            aim -= dist
        elif command == "down":
            aim += dist

    return (x_dist * aim, x_dist * y_dist)
