from operator import xor


def parse_input(inp: str) -> [[int]]:
    return [[int(c) for c in line] for line in inp.strip().split("\n")]


def bits_to_int(bits: [int]) -> int:
    result = 0
    for bit in bits:
        result = (result << 1) | bit

    return result


def commons(bins: [[int]], least=False) -> [int]:
    list_length = len(bins)
    bin_length = len(bins[0])
    result = []

    for i in range(bin_length):
        one_count = len(list(filter(lambda bin: bin[i] == 1, bins)))
        current_bit = 1 if xor(2 * one_count >= list_length, least) else 0
        result.append(current_bit)

    return result


def star1(inp: [[int]]):
    most_commons = commons(inp)
    least_commons = commons(inp, least=True)
    return bits_to_int(most_commons) * bits_to_int(least_commons)


def filter_bins(bins: [[int]], least=False) -> int:
    i = 0
    while len(bins) > 1:
        match = commons(bins, least=least)[i]
        bins = list(filter(lambda bin: bin[i] == match, bins))
        i += 1

    return bits_to_int(bins[0])


def star2(inp: [[int]]):
    return filter_bins(inp) * filter_bins(inp, least=True)


def solve(inp: str) -> (str, str):
    parsed_input = parse_input(inp)

    return (star1(parsed_input), star2(parsed_input))
