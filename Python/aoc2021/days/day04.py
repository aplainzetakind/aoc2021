def parseInput(input: str) -> ([int], [[int]]):
    blocks = input.strip().split("\n\n")
    nums = [int(n) for n in blocks[0].split(",")]
    boards = [[int(n) for n in block.split()] for block in blocks[1:]]

    return (nums, boards)


def checkBoard(board: [int]) -> int:
    WIN_PATTERNS = [list(range(i, i + 5)) for i in range(0, 25, 5)] + [
        list(range(i, i + 25, 5)) for i in range(5)
    ]

    for pattern in WIN_PATTERNS:
        if all([board[i] == -1 for i in pattern]):
            return sum([n for n in board if n != -1])

    return -1


def markBoard(board: [int], n: int) -> None:
    if n in board:
        board[board.index(n)] = -1


def solve(input: str) -> (str, str):
    nums, boards = parseInput(input)
    star1 = None
    res = -1
    i = 0
    while i < len(nums):
        j = 0
        while j < len(boards):
            markBoard(boards[j], nums[i])
            res = checkBoard(boards[j])
            if res != -1:
                if star1 is None:
                    star1 = res * nums[i]
                if len(boards) == 1:
                    return (str(star1), str(res * nums[i]))
                boards.pop(j)
                j -= 1
            j += 1
        i += 1
