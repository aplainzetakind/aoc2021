from collections import Counter


def parse_input(input: str) -> [((int, int), (int, int))]:
    result = []
    for line in input.strip().split("\n"):
        nums = [int(num) for num in ",".join(line.split(" -> ")).split(",")]
        result.append(((nums[0], nums[1]), (nums[2], nums[3])))

    return result


def explode(p1: (int, int), p2: (int, int), c: Counter, diag=False) -> None:
    x1, y1 = p1
    x2, y2 = p2

    if x1 == x2:
        c.update([(x1, y) for y in range(min(y1, y2), max(y1, y2) + 1)])
        return
    if y1 == y2:
        c.update([(x, y1) for x in range(min(x1, x2), max(x1, x2) + 1)])
        return
    if diag:
        x_indicator = 1 if x1 < x2 else -1
        y_indicator = 1 if y1 < y2 else -1
        c.update(
            zip(
                range(x1, x2 + x_indicator, x_indicator),
                range(y1, y2 + y_indicator, y_indicator),
            )
        )
        return


def solve(input: str) -> (str, str):
    ortho_counter = Counter()
    diag_counter = Counter()

    parsed_input = parse_input(input)
    for points in parsed_input:
        explode(*points, ortho_counter, False)
        explode(*points, diag_counter, True)

    star1 = sum([count > 1 for count in ortho_counter.values()])
    star2 = sum([count > 1 for count in diag_counter.values()])

    return (str(star1), str(star2))
