from collections import Counter

import numpy as np
from numpy.linalg import matrix_power


def day_elapse_matrix() -> np.array:
    return np.array(
        [
            [0, 1, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 1, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 1, 0, 0],
            [1, 0, 0, 0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0],
        ]
    )


def parse_input(input: str) -> np.array:
    return np.array([int(n) for n in input.strip().split(",")])


def run_days(n: int, arr: np.array) -> int:
    matrix = day_elapse_matrix()
    all_days = matrix_power(matrix, n)

    return sum(np.matmul(all_days, arr))


def solve(input: str) -> (str, str):
    counter = Counter({i: 0 for i in range(9)})
    counter.update(parse_input(input))

    init_counts = [count for count in counter.values()]

    star1 = run_days(80, init_counts)
    star2 = run_days(256, init_counts)

    return (str(star1), str(star2))
