from typing import Callable

import numpy as np


def parse_input(input: str) -> np.array:
    return np.array([int(n) for n in input.strip().split(",")])


def compute_cost(num: int, nums: [int], cost_func: Callable) -> int:
    return sum([cost_func(abs(num - x)) for x in nums])


def solve(input: str) -> (str, str):
    input = parse_input(input)
    median = int(np.median(input))
    mean = int(np.mean(input))
    star1 = compute_cost(median, input, lambda x: x)

    star2 = min(
        compute_cost(mean, input, lambda x: x * (x + 1) // 2),
        compute_cost(mean + 1, input, lambda x: x * (x + 1) // 2),
    )

    return (str(star1), str(star2))
