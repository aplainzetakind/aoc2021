from setuptools import find_packages, setup

setup(
    name="aoc2021-py",
    version="0.1",
    packages=find_packages(include=["aoc2021"]),
    include_package_data=True,
    install_requires=[
        "Click",
    ],
    entry_points="""
        [console_scripts]
        aoc2021-py=aoc2021:main
    """,
)
