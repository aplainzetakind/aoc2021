#!/bin/bash

# This script requires that the environment variable AOC_SESSION be set to the
# value of the session cookie for adventofcode.com

DAY=$(date +"%-d")

echo '\n'
for i in {60..1}
    do
        echo -e "\e[1A\e[KDownloading input in $i seconds..."
        sleep 1
    done

curl https://adventofcode.com/2021/day/${DAY}/input \
    --cookie "session=$AOC_SESSION" \
    > input/day${DAY}

echo Done.
